FROM openjdk:8
ADD target/insert.jar insert.jar
EXPOSE 80

ENV KAFKA_BOOTSTRAP_SERVERS=localhost:9092\
    DB_USERNAME=postgres\
    DB_PASSWORD=root\
    DB_NAME=metrica\
    DB_URL=localhost:5434

ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=prod", "insert.jar"]