CREATE TABLE metrica (
  timestamp TIMESTAMP,
  symbol VARCHAR (50),
  metrica FLOAT8
);


CREATE INDEX ON metrica (symbol, timestamp DESC);

SELECT create_hypertable(
  'metrica',
  'timestamp',
  chunk_time_interval => interval '1 day'
);
