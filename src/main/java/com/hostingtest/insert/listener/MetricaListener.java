package com.hostingtest.insert.listener;

import com.hostingtest.insert.enumeration.TopicEnum;
import com.hostingtest.insert.service.InsertService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

@Component
@AllArgsConstructor
public class MetricaListener {

    private InsertService insertService;

    @Transactional
    @KafkaListener(topics = "TEMPERATURE-1")
    public void temperatureListener(@Payload List<String> messages) {
        String[] row;
        Timestamp timestamp;
        Double metrica;
        for(String msg: messages) {
            row = msg.split("\\,");
            timestamp = new Timestamp(Long.parseLong(row[0]));
            metrica = Double.parseDouble(row[1]);
            insertService.insertMetrica(TopicEnum.TEMPERATURE, timestamp, metrica);
        }
    }

}
