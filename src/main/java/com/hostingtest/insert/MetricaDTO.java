package com.hostingtest.insert;

import com.hostingtest.insert.enumeration.TopicEnum;

import java.sql.Timestamp;

public class MetricaDTO {
    TopicEnum topic;
    Timestamp timestamp;
    Double metrica;
}
