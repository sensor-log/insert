package com.hostingtest.insert.service;

import com.hostingtest.insert.enumeration.TopicEnum;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
@AllArgsConstructor
public class InsertService {

    private JdbcTemplate jdbcTemplate;

    public void insertMetrica(TopicEnum topic, Timestamp timestamp, Double metrica) {
        jdbcTemplate.update("INSERT INTO metrica VALUES(?, ?, ?)",
                timestamp,
                topic.name(),
                metrica
        );
    }

}
